import requests
import logging
from flask import Flask
from flask import jsonify
from pysolar.solar import *
from datetime import datetime

app = Flask(__name__)
logger = logging.getLogger('werkzeug')
handler = logging.FileHandler('access.log')
logger.addHandler(handler)


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/sunrise/coordinates/<coordinates>', methods=['GET'])
def sunrise(coordinates):
    lat_lon = __get_lat_lon(coordinates)
    response = requests.get(
        'https://api.sunrise-sunset.org/json?lat=' + lat_lon[0] + '&lng=' + lat_lon[1] + '&formatted=0')
    timestamp = response.json()['results']['sunrise']
    time = timestamp.split('T')[1].split('+')[0]

    return jsonify({'sunrise': time})


@app.route('/sunset/coordinates/<coordinates>', methods=['GET'])
def sunset(coordinates):
    lat_lon = __get_lat_lon(coordinates)
    response = requests.get(
        'https://api.sunrise-sunset.org/json?lat=' + lat_lon[0] + '&lng=' + lat_lon[1] + '&formatted=0')
    timestamp = response.json()['results']['sunset']
    time = timestamp.split('T')[1].split('+')[0]

    return jsonify({'sunset': time})


@app.route('/solar-position/coordinates/<coordinates>', methods=['GET'])
def position(coordinates):
    lat_lon = __get_lat_lon(coordinates)
    lat = float(lat_lon[0])
    lon = float(lat_lon[1])
    date = datetime.datetime.now()

    positions = {
        'elevation': get_altitude(lat, lon, date),
        'azimuth': get_azimuth(lat, lon, date)
    }

    return jsonify(positions)


@app.route('/sunset', methods=['GET'])
def sunset_at_my_house():
    lat_lon = ['33.4484', '-112.0740']
    response = requests.get(
        'https://api.sunrise-sunset.org/json?lat=' + lat_lon[0] + '&lng=' + lat_lon[1] + '&formatted=0')

    sunrise = response.json()['results']['sunrise']
    sunrise_time = sunrise.split('T')[1].split('+')[0]
    sunrise_time = convert_hour(sunrise_time)

    sunset = response.json()['results']['sunset']
    sunset_time = sunset.split('T')[1].split('+')[0]
    sunset_time = convert_hour(sunset_time)

    date = sunrise.split('T')[0]

    now = datetime.now()

    is_sun_up = get_minute_of_day(sunrise_time.split(':')[0], sunrise_time.split(':')[1]) \
        < get_minute_of_day(now.hour, now.minute) \
        < get_minute_of_day(sunset_time.split(':')[0], sunset_time.split(':')[1])

    response_body = {
        'sunrise': sunrise_time,
        'sunset': sunset_time,
        'date': date,
        'isSunUp': is_sun_up}

    return jsonify(response_body)


@app.route('/current-date', methods=['GET'])
def current_time():
    now = datetime.now()
    current_date = now.strftime('%Y-%m-%d')
    current_hour = int(now.hour)
    current_min = int(now.minute)

    response_body = {
        'date': current_date,
        'hour': current_hour,
        'minute': current_min
    }

    return jsonify(response_body)


def get_minute_of_day(hour, minute):
    return (int(hour) * 60) + int(minute)


def convert_hour(time):
    hours_mins = time.split(':')
    hours_mins[0] = str((int(hours_mins[0]) + 17) % 24)
    return ':'.join(hours_mins)


def __get_lat_lon(coordinates):
    coordinates = coordinates.split(',')
    return [coordinates[0], coordinates[1]]

# if __name__ == '__main__':
#     app.run(debug=True)
